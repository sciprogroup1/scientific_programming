# -*- coding: utf-8 -*-
"""
    Author:         Birgit Bacher
    Purpose:        Decode "Caesar Cipher"
    Date:           Mon Oct  1 12:36:12 2018
    Arguments:      Via input()
    Outputs:        Via print()
    Dependencies:   none

"""


class caesar_cipher:
    """
    Class for decrypting Caesar Cipher
    Methods:
        __init__:               initialize object
        decrypt:                decrypt cipher
        print:                  output
        command_ine_input:      dummy_function for command line input
    """

    def __init__(self, string, *shift):
        self.string = string
        self.outstring = []
        if shift:  # shift known
            self.shift = shift[0]
            self.decrypt()
        else:  # shift not known
            cnt = 0
            while cnt < 26:
                self.shift = cnt
                self.decrypt()
                cnt += 1

    def decrypt(self):
        tempstring = ''
        for letter in self.string:
            if letter.isalpha():
                num = ord(letter)
                num += self.shift
                if letter.isupper():
                    if num > ord('Z'):
                        num -= 26
                    elif num < ord('A'):
                        num += 26
                elif letter.islower():
                    if num > ord('z'):
                        num -= 26
                    elif num < ord('a'):
                        num += 26
                tempstring += chr(num)
            else:
                tempstring += letter
        self.outstring.append(tempstring)

    def print(self):
        if len(self.outstring) == 1:
            tmpshift = self.shift
        else:
            tmpshift = 0
        for item in self.outstring:
            print("Decryption with shift %s: " % tmpshift)
            print(item)
            tmpshift += 1

    def command_line_input():
        """
        Get string to decrypt via console-input and call cesar_cipher class
        """
        print("Please insert decrypted string here: ")
        string = input()
        print("The input string is: \n", string)
        print("Please insert a shift if known, if not known press enter")
        shift = input()
        if shift:
            print("The entered shift is: ", shift)
            try:
                shift = int(shift)
                caesar = caesar_cipher(string, shift)
                caesar.print()
            except:
                print("Shift is not a number - trying various shifts....")
                caesar = caesar_cipher(string)
                caesar.print()
        else:
            print("No shift entered - trying various shifts....")
            caesar = caesar_cipher(string)
            caesar.print()

if __name__ == "__main__":
    caesar_cipher.command_line_input()
