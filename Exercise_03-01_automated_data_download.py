# -*- coding: utf-8 -*-
"""
    Author:         Birgit Bacher
    Purpose:        Automatically download SRTM data
    Date:           Sat Oct 20 14:12:13 2018
    Arguments:      Via Command line: left, bottom, right, top or: lon, lat
    Outputs:        File(s) in current direcory
    Dependencies:   none
    Functions:
        get_srtm_tile_name(**kwargs) - description in function
        get_srtm_tile(lat, lon) - file naming convention calculation
        check_existing_files(filelist) - returns a list of non existing files
        download_files(filelist) - downloads all listed files from base_url
"""

import math
import sys
import os
import urllib.request


base_url = 'http://srtm.csi.cgiar.org/SRT-ZIP/SRTM_V41/SRTM_Data_GeoTiff/'


def get_srtm_tile_name(**kwargs):
    """
        determines the filename(s) for the given grid/coordinats
        Input variant grid:
            call function like this:
            get_srtm_tile_name(left=left, bottom=bottom, right= right, top=top)
        Input variant single coordinate:
            call  function like this:
            get_srtm_tile_name(lon=lon, lat=lat)
    """
    filelist = []
    if 'left' in kwargs.keys():
        ileft, itop = get_srtm_tile(kwargs['left'], kwargs['top'])
        iright, ibottom = get_srtm_tile(kwargs['right'], kwargs['bottom'])
        for ilon in range(ileft, iright + 1):
            for ilat in range(itop, ibottom + 1):
                filelist.append('srtm_{:02d}_{:02d}.zip'.format(ilon, ilat))
    elif 'lon' in kwargs.keys():
        ilon, ilat = get_srtm_tile(kwargs['lon'], kwargs['lat'])
        filelist.append('srtm_{:02d}_{:02d}.zip'.format(ilon, ilat))
    else:
        print('wrong keys given')
        sys.exit(3)
    return filelist


def get_srtm_tile(lon, lat):
    ilon = int(math.floor(lon))
    ilat = int(math.floor(lat))
    if ilat > 59 or ilat < -59:
        print('latitude out of range')
    ilon = (ilon + 180) // 5 + 1
    ilat = (64 - ilat) // 5
    return ilon, ilat


def check_existing_files(filelist):
    new_filelist = []
    for file in filelist:
        if file in os.listdir():
            print(file, ' already exists... skipping download')
        else:
            new_filelist.append(file)
    if new_filelist == []:
        print('all files already exist... nothing to download')
        sys.exit(2)
    return new_filelist


def download_files(filelist):
    for file in filelist:
        print('downloading ' + file + ' ...')
        fullurl = base_url + file
        response = urllib.request.urlopen(fullurl)
        with open(file, 'b+w') as f:
            f.write(response.read())


if __name__ == "__main__":
    if len(sys.argv) == 5:
        left = float(sys.argv[1])
        bottom = float(sys.argv[2])
        right = float(sys.argv[3])
        top = float(sys.argv[4])
        filelist = get_srtm_tile_name(left=left, bottom=bottom, right=right, top=top)
    elif len(sys.argv) == 3:
        lon = float(sys.argv[1])
        lat = float(sys.argv[2])
        filelist = get_srtm_tile_name(lon=lon, lat=lat)
    else:
        print('wrong number of arguments given')
        sys.exit(1)
    filelist = check_existing_files(filelist)
    download_files(filelist)
