# import modules
from urllib.request import Request, urlopen
import json
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt


def control():
    # runs sub functions
    inn_data=dataload()
    year_temp, years, months = data_crunch(inn_data)
    # create 4 figures with 4 plots wanted
    plots(year_temp, years, months)


def dataload():
    # Parse the given url
    url = 'https://raw.githubusercontent.com/fmaussion/scientific_programming/master/data/innsbruck_temp.json'
    req = urlopen(Request(url)).read()
    # Read the data
    inn_data = json.loads(req.decode('utf-8'))
    # inn_data is a dictionary mapping strings 'YEAR', 'TEMP' and 'MONTH' to
    # lists with corresponding values
    return inn_data


def data_crunch(inn_data):
    # create numpy arrays out of inn_data
    temp = np.array(inn_data['TEMP'])
    year = np.array(inn_data['YEAR'])
    month = np.array(inn_data['MONTH'])

    # reshape arrays to get year in one and month in other dimension
    year_temp = np.reshape(temp, (int(len(temp) / 12), 12))
    year_year = np.reshape(year, (int(len(year) / 12), 12))
    year_month = np.reshape(month, (int(len(year) / 12), 12))

    # year vector for plots
    years = np.mean(year_year, axis=1)
    # month vector for plots
    months = year_month[0]
    # return output
    return year_temp, years, months


# create plots.
def plots(year_temp, years, months):
    
    # get index of 1981 and 2010 respectively
    start = list(years).index(1981)
    end = list(years).index(2010)
    # annual cycle
    temp_monthmean = np.mean(year_temp[start:end, ], axis=0)
    # create new figure and axes for plot
    fig1 = plt.figure(1)
    ax1 = plt.axes;
    # plot mean monthly annual cycle for 1981-2010
    plt.plot(months, temp_monthmean)
    # label axes and give title to the plot
    plt.ylabel('temperature (K)')
    plt.xlabel('month')
    plt.title('mean monthly annual cycle of temperature at Innsbruck for 1981-2010')

    # annual mean
    temp_yearmean = np.mean(year_temp, axis=1)
    # linear regression of temperature
    temp_slope, temp_intercept, temp_rvalue, temp_pvalue, temp_stderr = stats.linregress(years, temp_yearmean)
    # create new figure and axes for plot
    fig2 = plt.figure(2)
    ax2 = plt.axes;
    # plot mean annual temperature for 1977-2017 and linear trend
    plt.plot(years, temp_yearmean, 'b', label='annual mean')
    plt.plot(years, temp_intercept + temp_slope * years, 'r', label='linear trend')
    # label axes, create legend and give title to the plot
    plt.ylabel('temperature (K)')
    plt.xlabel('year')
    plt.legend(loc='best')
    plt.title('mean annual temperature time series at Innsbruck for 1981-2010')

    # annual mean for winter(12-2)
    temp_wintermean = np.mean(year_temp[:, [1, 2, 11]], axis=1)
    # linear regression of winter temperature
    wintemp_slope, wintemp_intercept, wintemp_rvalue, wintemp_pvalue, wintemp_stderr = stats.linregress(years, temp_wintermean)
    # create new figure and axes for plot
    fig3 = plt.figure(3)
    ax3 = plt.axes;
    # plot mean annual winter temperature for 1977-2017 and linear trend
    plt.plot(years, temp_wintermean, 'b', label='annual mean DJF')
    plt.plot(years, wintemp_intercept + wintemp_slope * years, 'r', label='linear trend')
    # label axes, create legend and give title to the plot
    plt.ylabel('temperature (K)')
    plt.xlabel('year')
    plt.legend(loc='best')
    plt.title('mean annual temperature time series for winter at Innsbruck for 1981-2010')
    
    # annual mean for summer (6-8)
    temp_summermean = np.mean(year_temp[:, 5:7], axis=1)
    # linear regression of summer temperature
    sumtemp_slope, sumtemp_intercept, sumtemp_rvalue, sumtemp_pvalue, sumtemp_stderr = stats.linregress(years, temp_summermean)
    # create new figure and axes for plot
    fig4 = plt.figure(4)
    ax4 = plt.axes;
    # plot mean annual summer temperature for 1977-2017 and linear trend
    plt.plot(years, temp_summermean, 'b', label='annual mean JJA')
    plt.plot(years, sumtemp_intercept + sumtemp_slope * years, 'r', label='linear trend')
    # label axes, create legend and give title to the plot
    plt.ylabel('temperature (K)')
    plt.xlabel('year')
    plt.legend(loc='best')
    plt.title('mean annual temperature time series for summer at Innsbruck for 1981-2010')
    

control()
