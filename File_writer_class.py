""" 
A Class wich writes an HTML-File
"""
import webbrowser

class html_writer:
    def __init__(self, file_path, header_title):
        header_sting = "<!DOCTYPE html> \n <html>\n <head>\n <title> {} </title>\n </head>\n <body>\n".format(header_title)
        self.file_path = file_path
        self.write_to_file(header_sting)
        
    def write_to_file(self, string):
        f = open(self.file_path, 'a')
        f.write(string)
        f.close
        
    def add_body_title(self, title, size=2):
        titlestr = "<h{}> {} </h{}>".format(size, title, size)
        self.write_to_file(titlestr)
        
    def add_paragraph(self, text):
        parstr = "<p> {} </p>".format(text)
        self.write_to_file(parstr)
        
    def add_image(self, imagepath):
        """Important: imagepath must be in double Gänsefüßchen, not in single"""
        imagestr = '<img alt="Image not found" src={}>'.format(imagepath)
        self.write_to_file(imagestr)
        
    def end_file(self):
        endstr = "</body> \n </html>"
        self.write_to_file(endstr)
        
    def display_html(self):
        webbrowser.open(self.file_path)
        
